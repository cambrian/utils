/************************************************************
 *  Copyright 2001, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global vars
var fso = new ActiveXObject("Scripting.FileSystemObject");
var counter = 0;
var ForReading = 1;

// Functions
function  updateCVSFolders(folderPath)
{
    var f = fso.GetFolder(folderPath);
    var fc = new Enumerator(f.SubFolders);
    for (; !fc.atEnd(); fc.moveNext())
    {
        var sfolder = fc.item();
        var fBase = fso.GetBaseName(sfolder);
        if(fBase == 'CVS') {
            //WScript.Echo("CVS found");
            counter++;

            var ff = new Enumerator(sfolder.files);
            for (; !ff.atEnd(); ff.moveNext())
            {
                var f1 = ff.item();
                var fileName = fso.GetBaseName(f1);
                /*
                //if(fileName == 'Repository' || fileName == 'Root') {
                if(fileName == 'Repository') {
                    var fileOldName = f1 + '.old';
                    f1.Copy (fileOldName);
                    var ts = fso.OpenTextFile(f1, ForReading);
                    var content = ts.ReadAll();
                    ts.Close();
                    var fileNewName = f1;
                    var hFile = fso.CreateTextFile(fileNewName, true);
                    //if(fileName == 'Repository') {
                        var tmp = content.split('cvs/');
                        var newcontent = tmp[1];
                        content = '/u/cvs/Projects/EnhydraOrg/apps/' + newcontent;
                    //} else {
                    //    content = ':pserver:anoncvs@enhydra.org:/u/cvs';
                    //}
                    hFile.WriteLine(content);
                    hFile.Close();
                    fso.DeleteFile(fileOldName);
                }
                */
            }
        } else {
            updateCVSFolders(sfolder);
        }
    }
}

// Main part
var pwd = ".";
//var pwd = "D:\\igc\\install";
updateCVSFolders(pwd);
WScript.Echo("" + counter + " folders updated.");

