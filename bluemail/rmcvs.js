/************************************************************
 *  Copyright 2001, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global vars
var fso = new ActiveXObject("Scripting.FileSystemObject");
var counter = 0;

// Functions
function  rmCVSFolders(folderPath)
{
    var f = fso.GetFolder(folderPath);
    var fc = new Enumerator(f.SubFolders);
    for (; !fc.atEnd(); fc.moveNext())
    {
        var fName = fc.item();
        var fBase = fso.GetBaseName(fName);
        if(fBase == 'CVS') {
            //WScript.Echo("CVS found");
            counter++;
            fso.DeleteFolder(fName);
        } else {
            rmCVSFolders(fName);
        }
    }
}

// Main part
var pwd = ".";
//var pwd = "D:\\igc\\install";
rmCVSFolders(pwd);
WScript.Echo("" + counter + " folders deleted.");

