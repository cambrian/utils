/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SendMsg.java,v 1.2 2001/03/25 06:50:20 vision Exp $ 

package com.bluecraft.bluemail;


import java.io.*;
import java.net.InetAddress;
import java.util.Properties;
import java.util.Date;

import javax.mail.*;
import javax.mail.internet.*;


/**
 * SendMsg sends an RFC822 (singlepart) message.
 * Usage: <code>java com.bluecraft.bluemail.SendMsg [[-L store-url] | [-T port] [-H host] [-U user] [-P passwd]]</code><BR>
 *        <code>[-s subject] [-o from-address] [-c cc-addresses] [-b bcc-addresses]</code><BR>
 *        <code>[-f record-mailbox] [-M transport-host] [-m message-body] [-d] [address]</code>
 *
 * @author   H. Yoon
 * @version  $Revision: 1.2 $
 */
public class SendMsg {
	private static boolean DEBUG = true;

    public static void main(String[] args) {
        new SendMsg(args);
    }

    public SendMsg(String[] args) {

        String to = null, subject = null, from = null, cc = null, bcc = null, url = null;
        String mailhost = null;
        String body = null;
        String mailer = "SendMsg";
        String protocol = null, host = null, user = null, password = null;
        String record = null;	// name of folder in which to record mail
        boolean debug = DEBUG;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int optind;

        for (optind = 0; optind < args.length; optind++) {
            if (args[optind].equals("-T")) {
                protocol = args[++optind];
            } else if (args[optind].equals("-H")) {
                host = args[++optind];
            } else if (args[optind].equals("-U")) {
                user = args[++optind];
            } else if (args[optind].equals("-P")) {
                password = args[++optind];
            } else if (args[optind].equals("-M")) {
                mailhost = args[++optind];
            } else if (args[optind].equals("-f")) {
                record = args[++optind];
            } else if (args[optind].equals("-s")) {
                subject = args[++optind];
            } else if (args[optind].equals("-m")) {
                body = args[++optind];
            } else if (args[optind].equals("-o")) { // originator
                from = args[++optind];
            } else if (args[optind].equals("-c")) {
                cc = args[++optind];
            } else if (args[optind].equals("-b")) {
                bcc = args[++optind];
            } else if (args[optind].equals("-L")) {
                url = args[++optind];
            } else if (args[optind].equals("-d")) {
                debug = true;
            } else if (args[optind].equals("--")) {
                optind++;
                break;
            } else if (args[optind].startsWith("-")) {
                usage();
                System.exit(1);
            } else {
                break;
            }
        }

        try {
            if (optind < args.length) {
                // XXX - concatenate all remaining arguments
                to = args[optind];
                System.out.println("To: " + to);
            } else {
                System.out.print("To: ");
                System.out.flush();
                to = in.readLine();
            }
            if (subject == null) {
                System.out.print("Subject: ");
                System.out.flush();
                subject = in.readLine();
            } else {
                System.out.println("Subject: " + subject);
            }

            Properties props = System.getProperties();
            // XXX - could use Session.getTransport() and Transport.connect()
            // XXX - assume we're using SMTP
            if (mailhost != null)
                props.put("mail.smtp.host", mailhost);
            if (debug)
                props.put("mail.debug", "true");

            // Get a Session object
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(debug);

            // construct the message
            Message msg = new MimeMessage(session);
            if (from != null)
                msg.setFrom(new InternetAddress(from));
            else
                msg.setFrom();

            msg.setRecipients(Message.RecipientType.TO,
                              InternetAddress.parse(to, false));
            if (cc != null)
                msg.setRecipients(Message.RecipientType.CC,
                                  InternetAddress.parse(cc, false));
            if (bcc != null)
                msg.setRecipients(Message.RecipientType.BCC,
                                  InternetAddress.parse(bcc, false));

            msg.setSubject(subject);

            if(body != null)
                msg.setText(body);
            else
                collect(in, msg);

            msg.setHeader("X-Mailer", mailer);
            msg.setSentDate(new Date());

            // send the thing off
            Transport.send(msg);
            System.out.println("\nMail was sent successfully.");

            // Keep a copy, if requested.
            if (record != null) {
                // Get a Store object
                Store store = null;
                if (url != null) {
                    URLName urln = new URLName(url);
                    store = session.getStore(urln);
                    store.connect();
                } else {
                    if (protocol != null)		
                        store = session.getStore(protocol);
                    else
                        store = session.getStore();

                    // Connect
                    if (host != null || user != null || password != null)
                        store.connect(host, user, password);
                    else
                        store.connect();
                }

                // Get record Folder.  Create if it does not exist.
                Folder folder = store.getFolder(record);
                if (folder == null) {
                    System.err.println("Can't get record folder.");
                    System.exit(1);
                }
                if (!folder.exists())
                    folder.create(Folder.HOLDS_MESSAGES);

                Message[] msgs = new Message[1];
                msgs[0] = msg;
                folder.appendMessages(msgs);

                System.out.println("Mail was recorded successfully.");
            }
        } catch (MessagingException mex) {
            System.out.println("\n--Exception handling in SendMsg.java");

            mex.printStackTrace();
            System.out.println();
            Exception ex = mex;
            do {
                if (ex instanceof SendFailedException) {
                    SendFailedException sfex = (SendFailedException)ex;
                    Address[] invalid = sfex.getInvalidAddresses();
                    if (invalid != null) {
                        System.out.println("    ** Invalid Addresses");
                        if (invalid != null) {
                            for (int i = 0; i < invalid.length; i++) 
                                System.out.println("         " + invalid[i]);
                        }
                    }
                    Address[] validUnsent = sfex.getValidUnsentAddresses();
                    if (validUnsent != null) {
                        System.out.println("    ** ValidUnsent Addresses");
                        if (validUnsent != null) {
                            for (int i = 0; i < validUnsent.length; i++) 
                                System.out.println("         "+validUnsent[i]);
                        }
                    }
                    Address[] validSent = sfex.getValidSentAddresses();
                    if (validSent != null) {
                        System.out.println("    ** ValidSent Addresses");
                        if (validSent != null) {
                            for (int i = 0; i < validSent.length; i++) 
                                System.out.println("         "+validSent[i]);
                        }
                    }
                }
                System.out.println();
                if (ex instanceof MessagingException)
                    ex = ((MessagingException)ex).getNextException();
                else
                    ex = null;
            } while (ex != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void collect(BufferedReader in, Message msg)
        throws MessagingException, IOException {
        String line;
        StringBuffer sb = new StringBuffer();
        while ((line = in.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }

        // If the desired charset is known, you can use
        // setText(text, charset)
        msg.setText(sb.toString());
    }

    private static void usage() {
        System.out.println("Usage: SendMsg [[-L store-url] | [-T port] [-H host] [-U user] [-P passwd]]");
        System.out.println("\t[-s subject] [-o from-address] [-c cc-addresses] [-b bcc-addresses]");
        System.out.println("\t[-f record-mailbox] [-M transport-host] [-m message-body] [-d] [address]");
    }

}
