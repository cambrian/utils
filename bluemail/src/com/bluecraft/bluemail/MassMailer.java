/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: MassMailer.java,v 1.2 2001/03/25 06:50:20 vision Exp $ 

package com.bluecraft.bluemail;


import java.io.*;
import java.net.InetAddress;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;


/**
 * MassMailer sends messages to the recipients specified in a file.
 * Usage: <code>java com.bluecraft.bluemail.MassMailer [-d] -t address-file -m message-file</code>
 *
 * @author   H. Yoon
 * @version  $Revision: 1.2 $
 */
public class MassMailer {
    static private String subject = "New release of Igc2000 (v1.2)";
    static private String from = "igc2000 <igc2000@yahoo.com>";
    static private String mailhost = "smtp.mail.yahoo.com";

    public static void main(String[] args) {
        new MassMailer(args);
    }

    public MassMailer(String[] args) {
        Random rand = new Random();

        String toFileName = null;
        String bodyFileName = null;
        boolean debug = false;

        int optind;
        for (optind = 0; optind < args.length; optind++) {
            if (args[optind].equals("-t")) {
                toFileName = args[++optind];
            } else if (args[optind].equals("-m")) {
                bodyFileName = args[++optind];
            } else if (args[optind].equals("-d")) {
                debug = true;
            } else if (args[optind].startsWith("-")) {
                usage();
                System.exit(1);
            } else {
                break;
            }
        }
        
        if ((toFileName == null) || (bodyFileName == null)) {
            usage();
            System.exit(1);
        }

        try {
            BufferedReader min = new BufferedReader(new FileReader(bodyFileName));
            String msgLine;
            StringBuffer msgBuff = new StringBuffer();
            while ((msgLine = min.readLine()) != null) {
                msgBuff.append(msgLine);
                msgBuff.append("\n");
            }
            String msg = msgBuff.toString();

            BufferedReader in = new BufferedReader(new FileReader(toFileName));
            String line;
            while ((line = in.readLine()) != null) {
                String to = null;
                String name = null;

                StringTokenizer stkn = new StringTokenizer(line, "\t");
                if(stkn.hasMoreTokens()) {
                    to = stkn.nextToken();
                    to.trim();
                }
                if(stkn.hasMoreTokens()) {
                    name = stkn.nextToken();
                    name.trim();
                }
                //System.out.println("SendMail: to=" + to + "; name=" + name);                

                if(to != null && to.length() > 0) {
                    String msgBody = "";
                    if(name != null && name.length() > 0) {
                        msgBody += "Dear " + name + ",\n\n";
                    } else {
                        msgBody += "Dear Igc2000 User,\n\n";
                    }
                    msgBody += msg;
                

                    String[] argArray = new String[9];
                    int idx = 0;
                    argArray[idx++] = "-M";
                    argArray[idx++] = mailhost;
                    argArray[idx++] = "-o";
                    argArray[idx++] = from;
                    argArray[idx++] = "-s";
                    if(subject != null)
                        argArray[idx++] = subject;
                    else
                        argArray[idx++] = "";
                    argArray[idx++] = "-m";
                    argArray[idx++] = msgBody;
                    argArray[idx++] = to;

                    for(int j=0;j<argArray.length;j++) {
                        System.out.println("Args: " + j + " = " + argArray[j]);
                    }

                    // Send this mail
                    new SendMsg(argArray);

                    // Sleep 10+r seconds.
                    long r = (long) (rand.nextFloat()*10000.0 + 1000.0);
                    Thread.sleep(10000L+r);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void usage() {
        System.out.println("Usage: java com.bluecraft.bluemail.MassMailer [-d] -t address-file -m message-file");
    }

}
