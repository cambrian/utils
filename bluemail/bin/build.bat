@echo off

:// Set classpath.
REM .\setcp

:// Compile java files.
echo Compiling java files...
javac ..\src\com\bluecraft\bluemail\*.java

:// Usage
REM echo Usage:
REM echo java com.bluecraft.bluemail.SendMsg -M smtp.mail.yahoo.com -o "\"igc2000\" <igc2000@yahoo.com>" -s test222 hyoungsoo@yahoo.com
REM echo java com.bluecraft.bluemail.MassMailer -t namelist.txt -m message.txt > abc.txt
